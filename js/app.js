App = Ember.Application.create();

App.Router.map(function () {
    this.resource('about');
    this.resource('wishlists', function () {
        this.resource('wishlist', {
            path: ':wishlist_id'
        })
    });
});

App.Adapter = DS.FixtureAdapter
App.Store = DS.Store.extend({
    revision: 12,
    adapter: App.Adapter.create()
});

App.Adapter.map('App.Wishlist', {
});

App.WishlistsRoute = Ember.Route.extend({
    model: function () {
        return App.Wishlist.find();
    },
});

App.WishlistController = Ember.ObjectController.extend({
    isEditing: false,
    edit: function () {
        this.set('isEditing', true);
    },
    doneEditing: function () {
        this.set('isEditing', false);
    },
    delete: function () {
        var content = this.get('content');
        content.deleteRecord();
        this.transitionTo('wishlists');
    },
})

App.IndexRoute = Ember.Route.extend({
    model: function () {}
});

App.Wishlist = DS.Model.extend({
    title: DS.attr('string'),
    description: DS.attr('string'),
    fireDate: DS.attr('date'),
    listItems: DS.hasMany('App.ListItem')
});

App.ListItem = DS.Model.extend({
    title: DS.attr('string'),
    description: DS.attr('string'),
    wishlist: DS.belongsTo('App.Wishlist')
});

App.ListItem.FIXTURES = [{
        id: 1,
        title: 'Streamers',
        description: 'yay!',
        wishlist: 1
    }, {
        id: 2,
        title: 'Balloons',
        description: 'fun with animals.',
        wishlist: 1
    }, {
        id: 3,
        title: 'Short back and sides',
        description: 'mind the ears',
        wishlist: 2
    }, {
        id: 4,
        title: 'Something for the weekend',
        description: 'extra large',
        wishlist: 2
    }
]

App.Wishlist.FIXTURES = [{
        id: 1,
        title: 'Do shopping',
        description: 'Big party tomorrow',
        fireDate: new Date('1991-05-29'),
        listItems: [1, 2]
    }, {
        id: 2,
        title: 'Get a haircut',
        description: 'Shaggy style!',
        fireDate: new Date('2013-01-27'),
        listItems: [3, 4]
    }
]

Ember.Handlebars.registerBoundHelper('date', function (date) {
    return moment(date).fromNow()
})
